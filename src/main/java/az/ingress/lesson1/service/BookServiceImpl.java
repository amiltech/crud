package az.ingress.lesson1.service;

import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.model.Book;
import az.ingress.lesson1.repository.BookRepository;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @PostConstruct
    private void init(){
        log.info("BookServiceImpl init");
    }
    @PreDestroy
    private void destroy(){
        log.info("BookServiceImpl destroy");
    }

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public int create(BookRequestDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .pageCount(dto.getPageCount())
                .name(dto.getName())
                .id(dto.getId())
                .build();
        bookRepository.save(book);
        return book.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto dto) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Book not found"));
        book.setAuthor(dto.getAuthor());
        book.setPageCount(dto.getPageCount());
        book.setName(dto.getName());
        book = bookRepository.save(book);
        return BookResponseDto.builder()
                .author(book.getAuthor())
                .pageCount(book.getPageCount())
                .name(book.getName())
                .id(book.getId())
                .build();
    }

    @Override
    public void delete(Integer id) {
        Optional<Book> optionalBook = bookRepository.findById(id);

        if (optionalBook.isPresent()) {
            bookRepository.deleteById(id);
        }
        System.out.println("Book is deleted succesfully with id " + id);

    }

    @Override
    public BookResponseDto get(Integer id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not found"));
        return BookResponseDto.builder()
                .author(book.getAuthor())
                .pageCount(book.getPageCount())
                .name(book.getName())
                .id(book.getId())
                .build();
    }

    @Override
    public List<BookResponseDto> getAll() {
        List<Book> all = bookRepository.findAll();
        List<BookResponseDto> bookResponseDtos = all.stream()
                .map(book -> BookResponseDto.builder()
                        .author(book.getAuthor())
                        .pageCount(book.getPageCount())
                        .name(book.getName())
                        .id(book.getId())
                        .build()
                )
                .collect(Collectors.toList());

        return bookResponseDtos;
    }
}

